const url = '/back';
const url_slider = '/back/slider.php';
const url_image_slider = "/var/www/html/enlistar/docs/";
const delay = 60 * 60 * 1000;

// Get data every hour

fetch(url, {
    method: 'GET',
    headers:{
        'Content-Type': 'application/json'
    }
}).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        console.log(response);
        var inTankToday = document.querySelector("#inTankToday h1");
        inTankToday.innerHTML = response.in_tank;
        var outLaundryToday = document.querySelector("#outLaundryToday h1");
        outLaundryToday.innerHTML = response.out_laundry;
        var relationOutLaundry = document.querySelector("#relationOutLaundry h1");
        relationOutLaundry.innerHTML = response.relation_in_out + "%";
        var consumptionMonth = document.querySelector("#consumptionMonth h1");
        consumptionMonth.innerHTML = response.consumption_current_month;
        var relationPrevMonth = document.querySelector("#relationPrevMonth h1");
        var img = (response.relation_consumption_month > 0) ? 'images/flecha._arriba.png' : 'images/flecha_abajo.png';
        relationPrevMonth.innerHTML = "<img src='"+img+"'/>" + Math.abs(parseInt(response.relation_consumption_month)) + "%";
        var maximumConsumptionHours = document.querySelector("#maximumConsumptionHours h1");
        maximumConsumptionHours.innerHTML = (response.max[0] !== undefined) ? (response.max[0] + " / " + response.max[1]) : "0 / 0";
    });

fetch(url_slider, {
    method: 'GET',
    headers:{
        'Content-Type': 'application/json'
    }
}).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
        let items = "";
        if(response.files >= 0 ){
            for (let i = 0; i<response.files; i++){
                items += '<div class="item dinamyc"><img src="'+ url_image_slider.toString() + (i+1).toString() +'.png" alt=""></div>';
            }
        }
        $('.carousel-inner').append(items);
    });

setTimeout(function getImages() {
    location.reload();
}, delay);
