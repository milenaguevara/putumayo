<?php

$servername = "localhost";
$username = "rita";
$password = "rita1234";
$dbname = "putumayo";
$tablename = "medicion";
$date_field = "fecha";
$in_tank_field = "entrada_tanque";
$in_laundry_field = "entrada_lavaderos";
$out_laundry_field = "salida_lavaderos";



function connectDB(){
	global $servername, $username, $password, $dbname;
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		return -1;
	}
	return $conn;
}

function closeConnection($conn){
	$conn->close();
}

function inTankToday(){
	global $tablename, $date_field, $in_tank_field;
	$sql = "SELECT " . $date_field . ", " . $in_tank_field ." FROM " . $tablename . " WHERE DATE(" . $date_field . ") = curdate() ORDER BY " . $date_field . " ASC;";
	try{
		$conn = connectDB();
		$result = $conn->query($sql);
		closeConnection($conn);
		if ($result->num_rows > 0) {
			$volume = 0;
			$dt = 0;
			$measurements = $result->fetch_all();
			for ($i=0; $i < count($measurements); $i++) {
				if($i != 0){
					$current_date = strtotime($measurements[$i][0]);
					$prev_date = strtotime($measurements[$i-1][0]);
					$dt = $current_date/60 - $prev_date/60;
		    		$volume += floatval($measurements[$i][1]) * $dt;
		    	}else{
		    		$volume += floatval($measurements[$i][1]);
		    	}
			}
			return round($volume, 2);
		} else {
		    return 0;
		}
	}catch(Exception $e){
		echo "Ha ocurrido un error";
	}
}

function outLaundryToday(){
	global $tablename, $date_field, $out_laundry_field;
	$sql = "SELECT " . $date_field . ", " . $out_laundry_field ." FROM " . $tablename . " WHERE DATE(" . $date_field . ") = curdate() ORDER BY " . $date_field . " ASC;";
	try{
		$conn = connectDB();
		$result = $conn->query($sql);
		closeConnection($conn);
		if ($result->num_rows > 0) {
			$volume = 0;
			$dt = 0;
			$measurements = $result->fetch_all();
			for ($i=0; $i < count($measurements); $i++) {
				if($i != 0){
					$current_date = strtotime($measurements[$i][0]);
					$prev_date = strtotime($measurements[$i-1][0]);
					$dt = $current_date/60 - $prev_date/60;
		    		$volume += floatval($measurements[$i][1]) * $dt;
		    	}else{
		    		$volume += floatval($measurements[$i][1]);
		    	}
			}
			return round($volume, 2);
		} else {
		    return 0;
		}
	}catch(Exception $e){
		echo "Ha ocurrido un error";
	}
}

function relationOutLaundry($in_tank, $out_laundry){
	if($in_tank != 0)
		return round(($out_laundry * 100)/$in_tank);
	return 0;
}

function maximumConsumptionHours(){
	global $tablename, $date_field, $in_laundry_field;
	$sql = "SELECT " . $date_field . ", " . $in_laundry_field ." FROM " . $tablename . " WHERE DATE(" . $date_field . ") = curdate() ORDER BY " . $in_laundry_field . " DESC limit 2;";
	try{
		$conn = connectDB();
		$result = $conn->query($sql);
		closeConnection($conn);
		if ($result->num_rows > 0) {
			$measurements = $result->fetch_all();
			$max = array();
			for ($i=0; $i < count($measurements); $i++) {
				$date = strtotime($measurements[$i][0]);
				array_push($max, date('H:i', $date));
			}
			return $max;
		} else {
		    return 0;
		}
	}catch(Exception $e){
		echo "Ha ocurrido un error";
	}
}

function consumptionMonth($month){
	global $tablename, $date_field, $in_tank_field;
	$sql = "SELECT " . $date_field . ", " . $in_tank_field ." FROM " . $tablename . " WHERE MONTH(" . $date_field . ") = " . $month . " AND YEAR(" . $date_field . ") = YEAR(CURRENT_DATE());";
	try{
		$conn = connectDB();
		$result = $conn->query($sql);
		closeConnection($conn);
		if ($result->num_rows > 0) {
			$volume = 0;
			$dt = 0;
			$measurements = $result->fetch_all();
			for ($i=0; $i < count($measurements); $i++) {
				if($i != 0){
					$current_date = strtotime($measurements[$i][0]);
					$prev_date = strtotime($measurements[$i-1][0]);
					$dt = $current_date/60 - $prev_date/60;
		    		$volume += floatval($measurements[$i][1]) * $dt;
		    	}else{
		    		$volume += floatval($measurements[$i][1]);
		    	}
			}
			return round($volume, 2);
		} else {
		    return 0;
		}
	}catch(Exception $e){
		echo "Ha ocurrido un error";
	}
}

function relationConsumptionMonth($current_month, $prev_month){
	if($current_month != 0)
		return round(($prev_month * 100)/$current_month);
	return 0;
}

// 'Litros que han entrado al tanque hoy';
$in_tank = inTankToday();

// 'Litros que han salido de los lavaderos hoy';
$out_laundry = outLaundryToday();

// '% Agua retornado a la quebrada hoy';
$return = relationOutLaundry($in_tank, $out_laundry);

// 'Horas pico de utilización de los lavaderos<br>';
$max = maximumConsumptionHours();

// 'Litros totales utilizados en la actividad de lavado por mes';
$consumptionCurrentMonth = consumptionMonth(date('m'));

// 'Aumento o disminución<br>';
$consumptionPrevMonth = consumptionMonth(date('m')-1);
$relationConsumption = relationConsumptionMonth($consumptionCurrentMonth, $consumptionPrevMonth) . "%";

echo json_encode(array('in_tank' => $in_tank, 'out_laundry' => $out_laundry, 'relation_in_out' => $return, 'max' => $max, 'consumption_current_month' => $consumptionCurrentMonth, 'relation_consumption_month' => $relationConsumption));


?>
